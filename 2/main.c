
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RED 12
#define GREEN 13
#define BLUE 14

void part_one();
void part_two();
int splitstring(const char* str, const char* delim, char*** tokens,
                int* token_size);

int main() {
  part_one();
  part_two();
  return 0;
}

void part_one() {
  FILE* fp;

  fp = fopen("input.txt", "r");
  if (fp == NULL) {
    printf("Could not open file\n");
    return;
  }
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  int total = 0;

  int game = 1;

  while ((read = getline(&line, &len, fp)) != -1) {
    int passed = 1;
    char** tokens;
    int token_size;
    int res = splitstring(line, ":", &tokens, &token_size);
    if (res != 0) {
      exit(EXIT_FAILURE);
    }

    for (int i = 1; i < token_size; i++) {
      char** reaches;
      int no_reaches;
      int res = splitstring(tokens[i], ";", &reaches, &no_reaches);
      if (res != 0) {
        exit(EXIT_FAILURE);
      }
      for (int x = 0; x < no_reaches; x++) {
        char** jewels;
        int no_jewels;
        int res = splitstring(reaches[x], ",", &jewels, &no_jewels);
        if (res != 0) {
          exit(EXIT_FAILURE);
        }
        for (int j = 0; j < no_jewels; j++) {
          char** gems;
          int no_gems;
          int res = splitstring(jewels[j], " ", &gems, &no_gems);
          if (res != 0) {
            exit(EXIT_FAILURE);
          }
          if ((strncmp(gems[1], "red", 3) == 0) && atoi(gems[0]) > RED) {
            passed = 0;
            break;
          } else if ((strncmp(gems[1], "green", 5) == 0) &&
                     atoi(gems[0]) > GREEN) {
            passed = 0;
            break;
          } else if ((strncmp(gems[1], "blue", 4) == 0) &&
                     atoi(gems[0]) > BLUE) {
            passed = 0;
            break;
          }
          for (int k = 0; k < no_gems; k++) {
            free(gems[k]);
          }
          free(gems);
        }
        for (int j = 0; j < no_jewels; j++) {
          free(jewels[j]);
        }
        free(jewels);
      }
      for (int x = 0; x < no_reaches; x++) {
        free(reaches[x]);
      }
      free(reaches);
    }

    for (int i = 0; i < token_size; i++) {
      free(tokens[i]);
    }
    free(tokens);
    if (passed) {
      total += game;
    }
    game++;
  }

  if (line) {
    free(line);
    line = NULL;
  }

  fclose(fp);
  printf("%d\n", total);
}

void part_two() {
  FILE* fp;

  fp = fopen("input.txt", "r");
  if (fp == NULL) {
    printf("Could not open file\n");
    return;
  }
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  int total = 0;

  int game = 1;

  while ((read = getline(&line, &len, fp)) != -1) {
    int passed = 1;
    char** tokens;
    int token_size;
    int game_total = 0;
    int res = splitstring(line, ":", &tokens, &token_size);
    if (res != 0) {
      exit(EXIT_FAILURE);
    }

    for (int i = 1; i < token_size; i++) {
      char** reaches;
      int no_reaches;
      int res = splitstring(tokens[i], ";", &reaches, &no_reaches);
      if (res != 0) {
        exit(EXIT_FAILURE);
      }
      int max_green = 0;
      int max_red = 0;
      int max_blue = 0;
      for (int x = 0; x < no_reaches; x++) {
        char** jewels;
        int no_jewels;
        int res = splitstring(reaches[x], ",", &jewels, &no_jewels);
        if (res != 0) {
          exit(EXIT_FAILURE);
        }
        for (int j = 0; j < no_jewels; j++) {
          char** gems;
          int no_gems;
          int res = splitstring(jewels[j], " ", &gems, &no_gems);
          if (res != 0) {
            exit(EXIT_FAILURE);
          }
          if ((strncmp(gems[1], "red", 3) == 0) && atoi(gems[0]) > max_red) {
            max_red = atoi(gems[0]);
          } else if ((strncmp(gems[1], "green", 5) == 0) &&
                     atoi(gems[0]) > max_green) {
            max_green = atoi(gems[0]);
          } else if ((strncmp(gems[1], "blue", 4) == 0) &&
                     atoi(gems[0]) > max_blue) {
            max_blue = atoi(gems[0]);
          }
          for (int k = 0; k < no_gems; k++) {
            free(gems[k]);
          }
          free(gems);
        }
        for (int j = 0; j < no_jewels; j++) {
          free(jewels[j]);
        }
        free(jewels);
      }
      for (int x = 0; x < no_reaches; x++) {
        free(reaches[x]);
      }
      free(reaches);
      game_total = max_red * max_blue * max_green;
    }

    for (int i = 0; i < token_size; i++) {
      free(tokens[i]);
    }
    free(tokens);
    total += game_total;
    game++;
  }

  if (line) {
    free(line);
    line = NULL;
  }

  fclose(fp);
  printf("%d\n", total);
}

int splitstring(const char* str, const char* delim, char*** tokens,
                int* token_size) {
  char* str_copy = (char*)malloc(strlen(str) + 1);
  strcpy(str_copy, str);
  char* ptr = strtok(str_copy, delim);
  *token_size = 0;
  *tokens = (char**)malloc(sizeof(char*));
  while (ptr != NULL) {
    *tokens = realloc(*tokens, sizeof(char**) * (*token_size + 1));
    (*tokens)[*token_size] = (char*)malloc(strlen(ptr) + 1);
    strcpy((*tokens)[*token_size], ptr);
    (*token_size)++;
    ptr = strtok(NULL, delim);
  }
  free(str_copy);

  return 0;
}
