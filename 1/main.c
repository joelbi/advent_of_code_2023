
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char numbers[10][10] = {"zero", "one", "two",   "three", "four",
                        "five", "six", "seven", "eight", "nine"};
void part_one();
void part_two();
int parse_number(char* line, ssize_t size);
int parse_number_backwards(char* line, ssize_t size);

int main() {
  part_one();
  part_two();
  return 0;
}

void part_one() {
  FILE* fp;

  fp = fopen("input.txt", "r");
  if (fp == NULL) {
    printf("Could not open file\n");
    return;
  }
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  int total = 0;

  while ((read = getline(&line, &len, fp)) != -1) {
    int first = 0;
    for (int i = 0; i < read; i++) {
      if (line[i] >= 0x30 && line[i] <= 0x39) {
        first = (int)(line[i] - 0x30);
        break;
      }
    }
    int last = 0;
    for (int i = read; i >= 0; i--) {
      if (line[i] >= 0x30 && line[i] <= 0x39) {
        last = (int)(line[i] - 0x30);
        break;
      }
    }
    total += ((first * 10) + last);
    if (line) {
      free(line);
      line = NULL;
      len = 0;
    }
  }

  printf("%d\n", total);
  fclose(fp);
}

void part_two() {
  FILE* fp;

  fp = fopen("input.txt", "r");
  if (fp == NULL) {
    printf("Could not open file\n");
    return;
  }
  char* line = NULL;
  size_t len = 0;
  ssize_t read;
  int total = 0;

  while ((read = getline(&line, &len, fp)) != -1) {
    int first = parse_number(line, read);
    int last = parse_number_backwards(line, read);

    total += ((first * 10) + last);
    if (line) {
      free(line);
      line = NULL;
      len = 0;
    }
  }

  printf("%d\n", total);
  fclose(fp);
}

int parse_number(char* line, ssize_t size) {
  for (int i = 0; i < size; i++) {
    if (line[i] >= 0x30 && line[i] <= 0x39) {
      return (int)(line[i] - 0x30);
    }
    for (int x = 0; x < 10; x++) {
      if (strncmp(line + i, numbers[x], strlen(numbers[x])) == 0) {
        return x;
      }
    }
  }
  return 0;
}

int parse_number_backwards(char* line, ssize_t size) {
  for (int i = 0; i < size; i++) {
    if (line[size - 1 - i] >= 0x30 && line[size - 1 - i] <= 0x39) {
      return (int)(line[size - 1 - i] - 0x30);
    }
    for (int x = 0; x < 10; x++) {
      if (strncmp(line + size - 1 - i, numbers[x], strlen(numbers[x])) == 0) {
        return x;
      }
    }
  }
  return 0;
}
